from time import sleep
import pycom
import config
import moteur as m
import http_server
from utime import ticks_ms
import wifi
import gc
from pysense import Pysense
import manager

# Set red light at start
pycom.heartbeat(False)
#pycom.rgbled(0x7f0000)

py = Pysense()
print("Raison du réveil: " + str(py.get_wake_reason()))
print("Voltage de la batterie: " + str(py.read_battery_voltage()))

try:
    errors = pycom.nvs_get('errors')
except:
    print("Erreurs depuis la NVS")
    pycom.nvs_set('errors', "")

config.add_exception(ticks_ms(), "Starting main...", True)

sleep(1)

m = config.start_thread(manager.manager())
h = config.start_thread(http_server.HTTP_SERVER(m))
w = config.start_thread(wifi.W())

#m=manager.manager()
#m.logs()
#m.set_moteurs()
#m.loop()
#for i in m.moteurs:
#    m.moteurs[i].ouvrir()
#    m.moteurs[i].fermer()

print("\rDémarré !")
#pycom.rgbled(False)
sleep(1)
