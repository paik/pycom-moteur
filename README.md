Dans config.py :

    Configurer le wifi :
    - Dans config_wifi : la liste des stations ou la configuration du point d'accès.

    Configurer les pins :
    - Dans config_pins.
    - Puis dans set_moteurs : créer les Pin, les attribuer à chaque moteur selon le type et le besoin.

    Configurer les moteurs :
    - Dans liste_moteurs : Ajouter un moteur avec la config nécessaire.
