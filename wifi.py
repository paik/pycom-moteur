from network import WLAN
from machine import idle
from time import sleep
#from uping import ping
from machine import reset
import config
import gc
from utime import ticks_ms

# Setting up the wifi
class W(object):

    def __init__(self):
        self.wlan = None

    def check(self):
        if self.wlan is not None and (self.wlan.isconnected() or config.config_wifi["wifi_mode"] == WLAN.AP) and self.wlan.ifconfig(id=config.config_wifi["wifi_mode"]-1)[0] != "0.0.0.0":
            if config.debug:
                print("wlan already connected")
        else:
            if config.debug:
                print("wlan NOT connected")
            WLAN().disconnect()
            WLAN().deinit()
            sleep(1)
            if config.config_wifi["wifi_mode"] == WLAN.STA:
                self.wlan = WLAN(mode=config.config_wifi["wifi_mode"])
                nets = self.wlan.scan()
                if config.debug:
                    print("Networks scanned : %s" % nets)
                for ssid in config.config_wifi["wifi_sta"]:
                    if config.debug:
                        print("Connect to %s with pwd %s" % (ssid, config.config_wifi["wifi_sta"][ssid]))
                    self.wlan.connect(ssid=ssid, auth=config.config_wifi["wifi_sta"][ssid])
                    i = 0
                    while not self.wlan.isconnected() and i < 5:
                        i += 1
                        if config.debug:
                            print("Trying to connect to wifi %s ... %s" % (ssid, i))
                        idle()
                        sleep(1)
                    if self.wlan.isconnected() and config.debug:
                        print("WiFi connected succesfully")
                        print(self.wlan.ifconfig())
                        break
                    elif config.debug:
                        print("WiFi %s not connected" % ssid)
            elif config.config_wifi["wifi_mode"] == WLAN.AP:
                self.wlan = WLAN()
                self.wlan.init(mode=config.config_wifi["wifi_mode"], ssid=config.config_wifi["wifi_ssid_ap"], auth=config.config_wifi["wifi_auth_ap"])
                sleep(2)
            if self.wlan.ifconfig(id=config.config_wifi["wifi_mode"]-1)[0] == "0.0.0.0":
                self.wlan = None
            else:
                config.add_exception(ticks_ms(), "Wifi connected, ip :" + str(self.wlan.ifconfig(id=config.config_wifi["wifi_mode"]-1)[0]), True)
        return self.wlan

    def loop(self, i = 0):
        try:
            #r = ping("192.168.1.1", quiet=True)
            #print(r[1])
            #if r[1] == 0:
            #    i += 1
            self.check()
            #else:
            #    i = 0
        except Exception as e:
            ee = "wifi loop error : " + str(e) + " - mem_alloc = " + str(gc.mem_alloc()) + " - mem_free = " + str(gc.mem_free())
            config.add_exception(ticks_ms(), ee, True)
            print(self.wlan)
            #self.check()
            i += 1
        sleep(10)
        return i

    def run(self):
        i = 0
        while True:
            i = self.loop(i)
            #print("ping errors : " + str(i))
            if i > 5:
                config.add_exception(ticks_ms(), "5 wifi check fails, resetting board", True)
                reset()
