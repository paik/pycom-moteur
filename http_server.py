import usocket
import _thread
from time import sleep
import config
import wifi
import gc
from utime import ticks_ms
from network import WLAN


class HTTP_SERVER(object):
    def __init__(self, t1):
        self.client_socket_error_count = 0
        self.server_socket_error_count = 0
        self.thread_1 = t1
        self.serversocket = None


    def loop(self):
        config.add_exception(ticks_ms(), "Starting server socket loop", True)
        while self.serversocket is None and not self.thread_1.stop:
            sleep(1)
            try:
                # Set up server socket
                if WLAN().isconnected() and WLAN().ifconfig(id=config.config_wifi["wifi_mode"]-1)[0] != "0.0.0.0":
                    self.serversocket = usocket.socket(usocket.AF_INET, usocket.SOCK_STREAM)
                    self.serversocket.setsockopt(usocket.SOL_SOCKET, usocket.SO_REUSEADDR, 1)
                    config.add_exception(ticks_ms(), "Bind serversocket, ip : " + str(WLAN().ifconfig(id=config.config_wifi["wifi_mode"]-1)[0]), True)
                    self.serversocket.bind((WLAN().ifconfig(id=config.config_wifi["wifi_mode"]-1)[0], 80))

                    # Accept maximum of 5 connections at the same time
                    self.serversocket.listen(5)

                    # Unique data to send back
                    self.c = 1
                    while True and not self.thread_1.stop:
                        # Accept the connection of the clients
                        (clientsocket, address) = self.serversocket.accept()
                        # Start a new thread to handle the client
                        _thread.start_new_thread(self.client_thread, (clientsocket, self.c))
                        self.c = self.c+1
                    self.serversocket.close()
                self.serversocket = None

            except Exception as e:
                self.server_socket_error_count += 1
                ee = "Serversocket error " + str(self.server_socket_error_count) + " : " + str(e) + " - mem_alloc = " + str(gc.mem_alloc()) + " - mem_free = " + str(gc.mem_free())
                config.add_exception(ticks_ms(), ee, True)
                if self.serversocket is not None:
                    self.serversocket.close()
                sleep(2)
                self.serversocket = None


    # Thread for handling a client
    def client_thread(self, clientsocket, n):
        try:
            # Receive maxium of 12 bytes from the client
            r = clientsocket.recv(4096)

            # If recv() returns with 0 the other end closed the connection
            if len(r) == 0:
                clientsocket.close()
                return
            else:
                # Do something wth the received data...
                #print("Received: {}".format(str(r))) #uncomment this line to view the HTTP request
                pass
            clientsocket.send(config.response(r, self.thread_1, n))

        except Exception as e:
            self.client_socket_error_count += 1
            ee = "Clientsocket error " + str(self.client_socket_error_count) + " : " + str(e) + " - mem_alloc = " + str(gc.mem_alloc()) + " - mem_free = " + str(gc.mem_free())
            config.add_exception(ticks_ms(), ee, True)

        if clientsocket is not None:
            # Close the socket and terminate the thread
            clientsocket.close()


    def run(self):
        while True and not self.thread_1.stop:
            sleep(1)
            try:
                self.loop()
            except Exception as e:
                self.serversocket = None
                ee = "Main error : " + str(e) + " - mem_alloc = " + str(gc.mem_alloc()) + " - mem_free = " + str(gc.mem_free())
                config.add_exception(ticks_ms(), ee, True)
