from math import isnan, trunc, isfinite
from utime import localtime
from pysense import Pysense
from SI7006A20 import SI7006A20
import pycom
import _thread
from utime import ticks_ms
from machine import reset
from network import WLAN
from machine import RTC
from machine import Pin, PWM
import moteur

py = Pysense()
si = SI7006A20(py)
rtc = RTC()
rtc.ntp_sync("pool.ntp.org")

version = 2.1
mode = {0:'auto', 1:'manuel', 2:'test'}
etat = {0:"inconnu", 1:"ouvert", -1:"fermé"}
messages = {}
debug = True
attente_inter_boucle_sync = 10


config_wifi = {
"wifi_mode": WLAN.AP, # WLAN.AP or WLAN.STA
"wifi_sta": {
'B': (WLAN.WPA2, "cotcotcotcot"),
'BuenosAyres': None,
},
"wifi_ssid_ap": 'P',
"wifi_auth_ap": (WLAN.WPA2, "cotcotcotcot"),
}

"""
PySense IO header :
EXT_IO0 = P2 (RGBLED)
EXT_IO1 = P9
EXT_IO2 = P10
EXT_IO3 = P11
EXT_IO4 = P4 (Cannot be used if SD card is being used)
EXT_IO5 = P8 (Cannot be used if SD card is being used)
"""
config_pins = {
"pin_ouvrir_str": 'P10',
"pin_fermer_str": 'P9',
"pin_direction_str": 'P11',
"pin_on_str": 'P12', #Fake pin
"pin_pwm_str": 'P4',
"pin_relais_str": {0: 'P8',},
}

liste_moteurs = {
    0:{
        "type": "imprimante",
        "nom": "Porte Poussins",
        "couleur": 0x00ff00, #vert
        "lux_ouverture": 1,
        "lux_fermeture": 1,
        "temps_ouverture": 2.5,
        "temps_fermeture": 2.5,
        "attente_inter_boucle_async": 10,
    },
    1:{
        "type": "verrin",
        "nom": "Porte Poules",
        "couleur": 0xff0000, #rouge
        "lux_ouverture": 1,
        "lux_fermeture": 1,
        "temps_ouverture": 60,
        "temps_fermeture": 60,
        "attente_inter_boucle_async": 10,
        "pwm_dt": 2,
        #Valeur du/des relais
        "relais": {0: 0,}
    },
    2:{
        "type": "verrin",
        "nom": "Mangeoires",
        "couleur": 0x0000ff, #bleu
        "lux_ouverture": 1,
        "lux_fermeture": 1,
        "temps_ouverture": 60,
        "temps_fermeture": 60,
        "attente_inter_boucle_async": 10,
        "pwm_dt": 2,
        #Valeur du/des relais
        "relais": {0: 1,}
    },
}

def set_moteurs():
    # Créer les Pin
    pin_ouvrir = Pin(config_pins["pin_ouvrir_str"], mode=Pin.OUT, value=0)
    pin_fermer = Pin(config_pins["pin_fermer_str"], mode=Pin.OUT, value=0)
    pin_direction = Pin(config_pins["pin_direction_str"], mode=Pin.OUT, value=0)
    pin_on = Pin(config_pins["pin_on_str"], mode=Pin.OUT, value=0)
    pwm = PWM(0, frequency=1000)  # use PWM timer 0, with a frequency of 5KHz
    pin_pwm = pwm.channel(0, pin=config_pins["pin_pwm_str"], duty_cycle=0)
    pin_relais = {}
    for relay in config_pins["pin_relais_str"]:
        pin_relais[relay] = Pin(config_pins["pin_relais_str"][relay], mode=Pin.OUT, value=0)

    # Donner accès aux Pin selon le besoin de chaque type de moteur
    moteurs = {}
    for i in liste_moteurs:
        liste_moteurs[i]["id"] = i
        print("Init moteur " + str(i))
        if liste_moteurs[i]["type"] == "imprimante":
            liste_moteurs[i]["pin_ouvrir"] = pin_ouvrir
            liste_moteurs[i]["pin_fermer"] = pin_fermer
            moteurs[i] = moteur.imprimante(liste_moteurs[i])
        elif liste_moteurs[i]["type"] == "verrin":
            liste_moteurs[i]["pin_direction"] = pin_direction
            liste_moteurs[i]["pin_on"] = pin_on
            liste_moteurs[i]["pin_pwm"] = pin_pwm
            liste_moteurs[i]["pin_relais"] = pin_relais
            moteurs[i] = moteur.verrin(liste_moteurs[i])

    print(moteurs)
    return moteurs

def add_exception(t, e, nvs=False):
    print(e)
    global messages
    i = len(messages)
    try:
        if i > 300:
            messages = {}
            i = len(messages)
        messages[i+1] = {'date' : rtc_to_str(rtc.now()), 'time' : ms_to_date_string(t), 'text' : e}
    except:
        messages = {}
        i = len(messages)
        messages[i+1] = {'date' : rtc_to_str(rtc.now()), 'time' : ms_to_date_string(t), 'text' : e}
    if nvs:
        try:
            errors = pycom.nvs_get('errors')
            errors += rtc_to_str(rtc.now()) + "-" + ms_to_date_string(t) + ' : ' + e + ', '
            if len(errors) >= 1984:
                errors = rtc_to_str(rtc.now()) + "-" + ms_to_date_string(t) + " : Reset NVS,"
                errors += rtc_to_str(rtc.now()) + "-" + ms_to_date_string(t) + ' : ' + e + ', '
            pycom.nvs_set('errors', errors)
        except:
            errors = rtc_to_str(rtc.now()) + "-" + ms_to_date_string(t) + " : Excepton - Reset NVS,"
            errors += rtc_to_str(rtc.now()) + "-" + ms_to_date_string(t) + ' : ' + e + ', '
            pycom.nvs_set('errors', errors)

def rtc_to_str(now):
    return str(now[0]) + "-" + str(now[1]) + "-" + str(now[2]) + " " + str(now[3]) + ":" + str(now[4]) + ":" + str(now[5])

def ms_to_date_string(ms):
    s = ""
    if not isnan(ms) and isfinite(ms):
        t = localtime(trunc(ms/1000.0))
        if t[7] > 1:
            s += str(t[7] - 1) + " jours " + str(t[3]) + " heures " + str(t[4]) + " min " + str(t[5]) + " sec"
        elif t[3] > 0:
            s += str(t[3]) + " heures " + str(t[4]) + " min " + str(t[5]) + " sec"
        elif t[4] > 0:
            s += str(t[4]) + " min " + str(t[5]) + " sec"
        else:
            s += str(t[5]) + " sec"
        return s
    return None

def start_thread(t, a=None):
    if a is None:
        _thread.start_new_thread(t.run, ())
    else:
        _thread.start_new_thread(t.run, (a))
    return t

def generer_form(dict, m=-1, prefix=None):
    message = '<form action="/changerConfig">'
    for c in dict:
        message += '<label for="' + str(c) + '">' + str(c) + ':</label>'
        if prefix is None:
            message += generer_type(dict[c], c)
        else:
            message += generer_type(dict[c], str(prefix) + "_" + str(c))
    if m > -1:
        message += '<input type="hidden" name=moteurId id=moteurId value="' + str(m) + '">'
        message += '<input type="reset" value="Reset"><input type="submit" value="Submit"> </form> <br>'
    return message

def generer_type(value, name):
    message = ''
    if type(value)==float:
        message += '<input type="number" required step="0.1" id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    elif type(value)==int and str(name)=="couleur":
        message += '<input type="color" required id="' + str(name) + '" name="' + str(name) + '" value=#' + '{:0>6}'.format(str(hex(value))[2:]) + '><br><br>'
    elif type(value)==int:
        message += '<input type="number" required step="1" id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    elif type(value)==str:
        message += '<input type="text" required readonly id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    elif type(value)==Pin:
        message += '<input type="text" required readonly id="' + str(name) + '" name="' + str(name) + '" value=' + str(value).replace(" ","") + '><br><br>'
    elif type(value)==dict:
        message += generer_form(value, prefix=name)
    else:
        print("autre type pour " + str(name) + " : " + str(type(value)))
        print(value)
        message += '<input type="text" required readonly id="' + str(name) + '" name="' + str(name) + '" value=' + str(value) + '><br><br>'
    return message

def response(r, t, n):
    message = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\nCache-Control: no-cache, no-store, must-revalidate\r\nPragma: no-cache\r\nExpires: 0\r\nConnection:close \r\n\r\n" #HTTP response
    '''
    head = '<head><meta name="viewport" content="width=device-width, initial-scale=1">'
    head += '<style>.switch {position: relative;  display: inline-block;  width: 60px;  height: 34px;}'
    head += '.switch input {   opacity: 0;  width: 0;  height: 0;}'
    head += '.slider {  position: absolute;  cursor: pointer;  top: 0;  left: 0;  right: 0;  bottom: 0;  background-color: #ccc;  -webkit-transition: .4s;  transition: .4s;}'
    head += '.slider:before {  position: absolute;  content: "";  height: 26px;  width: 26px;  left: 4px;  bottom: 4px;  background-color: white;  -webkit-transition: .4s;  transition: .4s;}'
    head += 'input:checked + .slider {  background-color: #2196F3;}'
    head += 'input:focus + .slider {  box-shadow: 0 0 1px #2196F3;}'
    head += 'input:checked + .slider:before {  -webkit-transform: translateX(26px);  -ms-transform: translateX(26px);  transform: translateX(26px);}'
    head += '.slider.round {  border-radius: 34px;}'
    head += '.slider.round:before {  border-radius: 50%;}'
    head += '</style></head>'
    '''
    head = ""

    if "GET / " in str(r):
        #this is a get response for the page
        # Sends back some data
        message += "<html>"
        message += head
        message += "<body><h1> Poulailler ("+ str(n) + ")</h1><br>"
        message += "<a href='/auto'><button>Mode auto</button></a><br>"
        message += "<a href='/manuel'><button>Mode manuel</button></a><br>"
        message += "<a href='/test'><button>Mode manuel et test porte</button></a><br>"
        message += "<a href='/ouvrir'><button>Ouvrir tous les moteurs</button></a><br>"
        message += "<a href='/fermer'><button>Fermer tous les moteurs</button></a><br>"
        message += "<a href='/infos'><button>Infos</button></a><br>"
        message += "<a href='/stop'><button>Stop</button></a><br>"
        message += "Mode : " + str(mode[t.mode]) + "<br>"
        message += "<br>"
        for m in t.moteurs:
            message += "Moteur " + str(t.moteurs[m].id) + " : " + str(t.moteurs[m].nom) + "<br>"
            message += "<a href='/ouvrir_" + str(m) + "'><button>Ouvrir</button></a><br>"
            message += "<a href='/fermer_" + str(m) + "'><button>Fermer</button></a><br>"
            message += "Etat = " + str(etat[t.moteurs[m].state]) + " depuis : " + str(ms_to_date_string(t.moteurs[m].chrono.read()*1000)) + "<br>"
            message += "<br>"
        message += "</body></html>"

    elif "GET /request_data " in str(r):
        message = "HTTP/1.1 200 OK\r\nContent-Type: application/json; charset=utf-8\r\nCache-Control: no-cache, no-store, must-revalidate\r\nPragma: no-cache\r\nExpires: 0\r\nConnection:close \r\n\r\n" #HTTP response
        message += '{"temperature": ' + str(si.temperature()) + ', "humidity": ' + str(si.humidity()) + ' }'
    elif "GET /infos " in str(r):
        message += "<html><body><h1> Ventilo ("+ str(n) + ")</h1><br>"
        message += "<a href='/request_data'><button>Request data</button></a><br>"
        message += "<a href='/reset'><button>Reset</button></a><br>"
        message += "<a href='/nvsmessages'><button>NVS et messages</button></a><br>"
        message += "<a href='/'><button>Retour</button></a><br>"
        message += "Version : " + str(t.version) + "<br>"
        message += "PySense allumé depuis " + str(ms_to_date_string(ticks_ms())) + "<br>"
        message += "Lumière (bleue en lux, rouge en lux): " + str(t.lumiere) + "<br>"
        message += "Température : " + str(si.temperature()) + " °C<br>"
        message += "Humidité : " + str(si.humidity()) + " %RH<br>"
        message += "Tension batterie : " + str(py.read_battery_voltage()) + " V<br>"
        message += "<br>"
        for m in t.moteurs:
            message += "Moteur " + str(t.moteurs[m].id) + " : " + str(t.moteurs[m].nom) + "<br>"
            message += "Limite lumière rouge+bleue en lux ouverture : " + str(t.moteurs[m].lux_ouverture) + ", fermeture : " + str(t.moteurs[m].lux_fermeture) + "<br>"
            message += "Temps ouverture : " + str(t.moteurs[m].temps_ouverture) + ", fermeture : " + str(t.moteurs[m].temps_fermeture) + "<br>"
            message += "Attente inter boucle : " + str(t.moteurs[m].attente_inter_boucle_async) + " sec<br>"
            message += "Etat = " + str(etat[t.moteurs[m].state]) + " depuis : " + str(ms_to_date_string(t.moteurs[m].chrono.read()*1000)) + "<br>"
            message += "Précédemment " + str(etat[-t.moteurs[m].state]) + " depuis : " + str(ms_to_date_string(t.moteurs[m].dernier_chrono*1000)) + "<br>"
            #for c in liste_moteurs[m]:
            #    message += str(c) + " : " + str(liste_moteurs[m][c]) + "<br>"
            message += generer_form(liste_moteurs[m], m)
            message += "<br>"
        message += "</body></html>"
    elif "GET /nvsmessages " in str(r):
        message += "<html><body><h1> Ventilo ("+ str(n) + ")</h1><br>"
        message += "<a href='/erase'><button>Erase NVS</button></a><br>"
        message += "<a href='/'><button>Retour</button></a><br>"
        message += "NVS errors : " + "<br>"
        for k in pycom.nvs_get('errors').split(','):
            message += k + "<br>"
        message += "Messages :<br>"
        j = len(messages)
        for i in messages:
            if j < len(messages) - 200:
                message += "Messages truncated (more than 200)<br>"
                break
            message += str(j) + " - " + str(messages[j]['date']) + " - " + str(messages[j]['time']) + " : " + str(messages[j]['text']) + "<br>"
            j -= 1
        message += "</body></html>"
    elif "GET /auto " in str(r):
        t.auto()
        add_exception(ticks_ms(), "Mode auto")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /manuel " in str(r):
        t.manu()
        add_exception(ticks_ms(), "Mode manuel")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /test " in str(r):
        t.test()
        t.test()
        add_exception(ticks_ms(), "Mode manuel et test porte")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /ouvrir " in str(r):
        #pycom.rgbled(0xFFFFFF)
        t.ouvrir()
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /ouvrir" in str(r):
        #pycom.rgbled(0xFFFFFF)
        try:
            moteur_id = int(str(r).split(" ")[1].split("_")[1])
            print("ouvrir moteur %s" % moteur_id)
            t.moteurs[moteur_id].ouvrir()
        except:
            pass
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /fermer " in str(r):
        #pycom.rgbled(0xFFFFFF)
        t.fermer()
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /fermer" in str(r):
        #pycom.rgbled(0xFFFFFF)
        try:
            moteur_id = int(str(r).split(" ")[1].split("_")[1])
            print("fermer moteur %s" % moteur_id)
            t.moteurs[moteur_id].fermer()
        except:
            pass
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /stop " in str(r):
        t.stop = 1
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /erase " in str(r):
        pycom.nvs_set('errors', "")
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /reset " in str(r):
        reset()
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"
    elif "GET /changerConfig" in str(r):
        try:
            configs=str(r).split(" ")[1].split("?")[1].split("&")
            out={}
            for c in configs:
                config = c.split("=")
                out[str(config[0])] = str(config[1])
            id = int(out['moteurId'])
            for c in liste_moteurs[id]:
                if c in out:
                    if type(liste_moteurs[id][c])==float:
                        liste_moteurs[id][c]=float(out[c])
                    elif type(liste_moteurs[id][c])==int and str(c)=="couleur":
                        liste_moteurs[id][c]=int('0x' + str(out[c])[3:])
                    elif type(liste_moteurs[id][c])==int:
                        liste_moteurs[id][c]=int(out[c])
                    elif type(liste_moteurs[id][c])==str:
                        liste_moteurs[id][c]=str(out[c])
                    elif type(liste_moteurs[id][c])==dict:
                        for c2 in liste_moteurs[id][c]:
                            if type(liste_moteurs[id][c][c2])==int:
                                liste_moteurs[id][c][c2]=int(out[str(c) + "_" + str(c2)])
        except Exception as e:
            add_exception(ticks_ms(), e, True)
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/infos \r\n\r\n"
    elif "GET /" in str(r):
        message = "HTTP/1.1 307 Temporary Redirect\r\nLocation:/ \r\n\r\n"

    return message
