import config
from utime import ticks_ms
from time import sleep
from LTR329ALS01 import LTR329ALS01
from pysense import Pysense
py = Pysense()

class manager(object):
    def __init__(self):
        self.moteurs = config.set_moteurs()
        self.stop = 0
        self.attente_inter_boucle_sync = config.attente_inter_boucle_sync
        self.mode = 0
        self.version = config.version

        self.lt = LTR329ALS01(py)
        self.lumiere = self.lt.light()

    def logs(self):
        print("PySense allumé depuis " + str(config.ms_to_date_string(ticks_ms())))

    def loop(self, force=True):
        self.lumiere = self.lt.light()
        for m in self.moteurs:
            try:
                self.moteurs[m].get_config(config.liste_moteurs[m])
            except Exception as e:
                print(e)
            if not self.moteurs[m].stop and (force or self.moteurs[m].mode == 0):
                self.moteurs[m].loop()
            elif not self.moteurs[m].stop and self.moteurs[m].mode == 2:
                self.moteurs[m].test()

    def run(self):
        while True and not self.stop:
            self.loop(False)
            sleep(self.attente_inter_boucle_sync)

    def auto(self):
        self.mode = 0
        for m in self.moteurs:
            self.moteurs[m].mode = 0

    def manu(self):
        self.mode = 1
        for m in self.moteurs:
            self.moteurs[m].mode = 1

    def test(self):
        self.mode = 2
        for m in self.moteurs:
            self.moteurs[m].mode = 2

    def ouvrir(self):
        for m in self.moteurs:
            self.moteurs[m].ouvrir()

    def fermer(self):
        for m in self.moteurs:
            self.moteurs[m].fermer()
