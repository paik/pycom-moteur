from utime import ticks_ms
from machine import Timer
import pycom
from time import sleep
import config
from LTR329ALS01 import LTR329ALS01
from pysense import Pysense
py = Pysense()

config_defaut = {
"nom": "nom",
"lux_ouverture": 1,
"lux_fermeture": 1,
"temps_ouverture": 40,
"temps_fermeture": 40,
"attente_inter_boucle_async": 10,
"pin_ouvrir": None,
"pin_fermer": None,
"pin_direction": None,
"pin_on": None,
"pin_pwm": None,
"pwm_dt": 2
}


class moteur(object):
    def __init__(self, config_moteur=config_defaut):
        try:
            state = pycom.nvs_get('state_' + str(self.id))
            print("Etat depuis la NVS : " + str(state))
        except Exception as e:
            print(e)
            print("ERREUR durant la lecture de l'état de la NVS !!! Etat mis à 0...")
            state = 0
        # Setting up chrono to keep 2 last changes
        self.dernier_chrono = 0
        self.chrono = Timer.Chrono()
        self.chrono.start()
        # Light sensor
        self.lt = LTR329ALS01(py)
        self.lumiere = self.lt.light()
        self.version = config.version
        self.state = state
        self.temp_state = 0
        self.mode = 0
        self.stop = 0
        self.get_config(config_moteur)

    def get_config(self, config_moteur=config_defaut):
        self.nom = str(config_moteur["nom"])
        self.id = str(config_moteur["id"])
        self.couleur = config_moteur["couleur"]
        self.lux_ouverture = config_moteur["lux_ouverture"]
        self.lux_fermeture = config_moteur["lux_fermeture"]
        self.temps_ouverture = config_moteur["temps_ouverture"]
        self.temps_fermeture = config_moteur["temps_fermeture"]
        self.attente_inter_boucle_async = config_moteur["attente_inter_boucle_async"]

    def led_ouvrir(self):
        pycom.rgbled(self.couleur)
        sleep(2)
        pycom.rgbled(False)

    def led_fermer(self):
        pycom.rgbled(self.couleur)
        sleep(0.5)
        pycom.rgbled(False)
        sleep(0.5)
        pycom.rgbled(self.couleur)
        sleep(0.5)
        pycom.rgbled(False)
        sleep(0.5)

    def run(self):
        while True and not self.stop:
            if self.mode == 0:
                self.loop()
                sleep(self.attente_inter_boucle_async)
            elif self.mode == 2:
                self.test()
            else:
                sleep(1)

    def loop(self):
        self.lumiere = self.lt.light()
        #Getting last state from non volatile storage
        if config.debug:
            print("Moteur : " + str(self.nom))
            print("Lumière (bleue en lux, rouge en lux): " + str(self.lumiere))
            print("Limite lumière rouge+bleue en lux ouverture : " + str(self.lux_ouverture) + ", fermeture : " + str(self.lux_fermeture))
            print("Temps ouverture : " + str(self.temps_ouverture) + ", fermeture : " + str(self.temps_fermeture))
            print("Attente inter boucle : " + str(self.attente_inter_boucle_async) + " sec")
            print("Etat = " + str(config.etat[self.state]) + " depuis " + str(config.ms_to_date_string(self.chrono.read()*1000)))
            print("Précédemment " + str(config.etat[-self.state]) + " depuis " + str(config.ms_to_date_string(self.dernier_chrono*1000)))
            print("\r\n")
        if self.state != 1 and self.lumiere[1] > self.lux_ouverture:
            self.ouvrir()
        elif self.state != -1 and self.lumiere[1] < self.lux_fermeture:
            self.fermer()

    def test(self):
        sleep(1)
        self.ouvrir()
        sleep(1)
        self.fermer()


class imprimante(moteur):

    def __init__(self, config_moteur=config_defaut):
        if config_moteur["type"] != "imprimante":
            raise NameError("Mauvais type de moteur, attendu imprimante")


        super(imprimante, self).__init__(config_moteur=config_moteur)

        # Setting up pins
        # droite/close/state = -1
        # gauche/open/state = 1
        self.pin_ouvrir = config_moteur["pin_ouvrir"]
        self.pin_fermer = config_moteur["pin_fermer"]

    def ouvrir(self):
        if self.temp_state == 0:
            self.led_ouvrir()
            self.pin_ouvrir(0)
            self.pin_fermer(0)
            sleep(1)

            self.temp_state = 1
            print("Ouverture de " + str(self.nom))
            print("Lumière rouge+bleu en lux = " + str(self.lumiere[0] + self.lumiere[1]))
            self.chrono.stop()
            self.dernier_chrono = self.chrono.read()
            print("Chrono : " + str(self.dernier_chrono/3600))
            self.pin_ouvrir(1)
            sleep(0.1)
            self.pin_ouvrir(0)
            sleep(1)
            self.pin_ouvrir(1)
            sleep(self.temps_ouverture)
            self.pin_ouvrir(0)
            sleep(1)
            self.state = 1
            pycom.nvs_set('state_' + str(self.id), self.state)
            config.add_exception(ticks_ms(), "Ouverture de " + str(self.nom) + " " + str(self.lumiere[0] + self.lumiere[1]) + " lux")
            self.chrono.reset()
            self.chrono.start()
            self.temp_state = 0
        else:
            print("Pas ouvrir : en train de bouger ?")

    def fermer(self):
        if self.temp_state == 0:
            self.led_fermer()
            self.pin_ouvrir(0)
            self.pin_fermer(0)
            sleep(1)

            self.temp_state = -1
            print("Fermeture de " + str(self.nom))
            print("Lumière rouge+bleu en lux = " + str(self.lumiere[0] + self.lumiere[1]))
            self.chrono.stop()
            self.dernier_chrono = self.chrono.read()
            print("Chrono : " + str(self.dernier_chrono/3600))
            self.pin_fermer(1)
            sleep(0.1)
            self.pin_fermer(0)
            sleep(1)
            self.pin_fermer(1)
            sleep(self.temps_fermeture)
            self.pin_fermer(0)
            sleep(1)
            self.state = -1
            pycom.nvs_set('state_' + str(self.id), self.state)
            config.add_exception(ticks_ms(), "Fermeture de " + str(self.nom) + " " +  str(self.lumiere[0] + self.lumiere[1]) + " lux")
            self.chrono.reset()
            self.chrono.start()
            self.temp_state = 0
        else:
            print("Pas fermer : en train de bouger ?")


class verrin(moteur):

    def __init__(self, config_moteur=config_defaut):
        if config_moteur["type"] != "verrin":
            raise NameError("Mauvais type de moteur, attendu verrin")

        super(verrin, self).__init__(config_moteur=config_moteur)

        # Setting up pins
        # droite/close/state = -1
        # gauche/open/state = 1
        self.pin_direction = config_moteur["pin_direction"]
        self.pin_on = config_moteur["pin_on"]
        self.pin_relais = config_moteur["pin_relais"]
        self.pwm_c = config_moteur["pin_pwm"]
        self.pwm_dt = config_moteur["pwm_dt"]
        self.valeur_relais = config_moteur["relais"]

    def bouger(self, direction, texte):
        if self.temp_state == 0:
            self.pin_on(0)
            self.pin_direction(direction)
            self.pwm_c.duty_cycle(0)
            for i in self.pin_relais:
                print("setting relais %s to %s" % (self.pin_relais[i], self.valeur_relais[i]))
                self.pin_relais[i](self.valeur_relais[i])
            sleep(1)

            self.temp_state = 1
            print(str(texte) + " de " + str(self.nom))
            print("Lumière rouge+bleu en lux = " + str(self.lumiere[0] + self.lumiere[1]))
            self.chrono.stop()
            self.dernier_chrono = self.chrono.read()
            print("Chrono : " + str(self.dernier_chrono/3600))
            self.pin_on(1)
            for i in range(0, 101):
                self.pwm_c.duty_cycle(i/100)
                sleep(self.pwm_dt/100)
            sleep(self.temps_ouverture)
            for i in range(0, 101):
                self.pwm_c.duty_cycle(1 - i/100)
                sleep(self.pwm_dt/100)
            self.pin_on(0)
            for i in self.pin_relais:
                print("setting relais %s to %s" % (self.pin_relais[i], 0))
                self.pin_relais[i](0)
            sleep(1)
            if direction == 0:
                self.state = 1
            elif direction == 1:
                self.state = -1
            pycom.nvs_set('state_' + str(self.id), self.state)
            config.add_exception(ticks_ms(), str(texte) + " de " + str(self.nom) + " " +  str(self.lumiere[0] + self.lumiere[1]) + " lux")
            self.chrono.reset()
            self.chrono.start()
            self.temp_state = 0
        else:
            print("Ne peut pas ouvrir : en train de bouger ?")

    def ouvrir(self):
        self.led_ouvrir()
        self.bouger(0, "Ouverture")

    def fermer(self):
        self.led_fermer()
        self.bouger(1, "Fermeture")
